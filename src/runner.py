from network import *
from architectures_and_parameters import params_options
import copy


training_set = np.genfromtxt('../data/DATA_TRAIN.csv', delimiter=',')
validation_set = np.genfromtxt('../data/DATA_valid.csv', delimiter=',')

# recives n(num of iterations to average) and the relevant parameters
# returns the average accuracy of thos paramaters over n iterations
def get_average_accuracy(n, network_architecture, hyper_parameters, training_set, validation_set):
    accuracy_sum = 0
    for i in range(n):
        print("in itereation " + str(i) + " out of " + str(n))
        current_weights_biases = train_network(network_architecture, hyper_parameters, training_set)
        accuracy_sum += validate_network(network_architecture, current_weights_biases, validation_set)

    average_accuracy = accuracy_sum / n
    print("average accuracy is: " + str(average_accuracy))
    return average_accuracy

# recives the network architecture,hyper paraemeters, and training set
# returns a dictionary of the learned weights and biases
def train_network(network_architecture, hyper_parameters, training_set):
    current_weights_biases = hyper_parameters["init_function"](network_architecture)

    previous_weights_deltas = 0
    for i in range(hyper_parameters["num_iterations"]):
        np.random.shuffle(training_set)
        for example in training_set:
            input_vector = np.array([example[0], example[1]])
            sigma0 = np.array(example[2])

            output, forward_history = feet_forward(input_vector, current_weights_biases, network_architecture)

            weights_biases_changes = back_prop(sigma0, network_architecture, forward_history, hyper_parameters["der_error_function"],
                                               hyper_parameters["regul_function"], hyper_parameters["regul_lambda"])

            if not hyper_parameters['momentum_rate'] == 0 and not previous_weights_deltas == 0:
                weights_biases_changes = update_momentum_deltas(weights_biases_changes, previous_weights_deltas, hyper_parameters['momentum_rate'])

            current_weights_biases = update_weights_biases(current_weights_biases, weights_biases_changes,
                                                           hyper_parameters["learning_rate"])



            previous_weights_deltas = copy.deepcopy(weights_biases_changes)
            loss = hyper_parameters["error_function"](output, sigma0)
            print(loss)

    return current_weights_biases

# recives the network architecture, it's weights and biases, and a validation set
# prints (and returns) the accuracy of the network which will be defined by (correct_predictions / num of examples)
def validate_network(network_architecture, weights_biases, validation_set):
    num_examples = len(validation_set)
    correct_predictions = 0
    np.random.shuffle(validation_set)

    for example in validation_set:
        input_vector = np.array([example[0], example[1]])
        sigma0 = np.array(example[2])

        output, _ = feet_forward(input_vector, weights_biases, network_architecture)

        prediction = 1 if output >= 0.5 else 0

        if prediction == sigma0:
            correct_predictions += 1

    accuracy = correct_predictions / num_examples
    print("the network's accuracy is: " + str(accuracy))
    return accuracy


if __name__ == '__main__':
    network_arch = params_options["option1"]['network_architecture']
    hyper_params = params_options['option1']

    weights_biases = train_network(network_arch, hyper_params, training_set)
    accuracy = validate_network(network_arch,weights_biases,validation_set)
