from helper_functions import *


# recives input vector,list of matrices of weights, and the archiecture of the network
# returns the output of the network(sigma), and a 2d array of the neuron values
def feet_forward(input_vector, weights_biases, network_architecture):
    current_input = input_vector
    forward_history = {}
    num_layer = len(network_architecture) - 1

    for layer_index in range(num_layer):
        layer_index += 1 # the correct index is plus 1
        # print ("current input is: " + str(current_input))
        # print ("current weight matrix is: " + str(weights_biases["W" + str(layer_index)]))
        # print ("current bias vector is: " + str(weights_biases["b" + str(layer_index)]))

        current_activation_function = network_architecture[layer_index - 1]['activation'] # need the index of the previous layer, to ge

        try:
            current_weights = weights_biases["W" + str(layer_index)]
        except:
            print("error")
        current_biases = weights_biases["b" + str(layer_index)]
        current_input,layer_forward_history = single_layer_forward(current_input, current_weights, current_biases, current_activation_function)
        forward_history["layer" + str(layer_index)] = layer_forward_history

    network_output = current_input
    return network_output, forward_history

# recives input vector(A), weights corresponding to the input, and the activation function for this layer
# returns the output vector (Z), and computional history for this layer, to later be used in backprop
def single_layer_forward(input_vector, layer_weights, layer_biases, activation_function):
    linear_output = (layer_weights.dot(input_vector)) + layer_biases
    linear_output = linear_output[0,:] # need only one row
    activation_output = activation_function(linear_output)


    layer_history = {"input": input_vector, "weights" : layer_weights, "biases":layer_biases, "linear_output":linear_output, "activation_output": activation_output}

    return activation_output, layer_history

# recives sigma0, the network architecture, the feet forward history and the derivative function
# of the error function
# returns a dictionary for the changes for each layer's weights and biases
def back_prop(sigma0, network_architecture, forward_history, der_error_function, regulziation_function, regul_labmda):
    changes = {}
    current_layer_index = len(forward_history)
    current_layer_history = forward_history["layer" + str(current_layer_index)]

    d_error = der_error_function(current_layer_history["activation_output"], sigma0) # derivative of the error with respect to output

    current_activation_function_deriv = network_architecture[current_layer_index - 1]["deriv_activation"]

    dW, db, curr_dA = single_layer_backprop(d_error, current_layer_history, current_activation_function_deriv)

    if not regul_labmda == 0:
        dW += regulziation_function(current_layer_history["weights"], regul_labmda)

    changes["dW" + str(current_layer_index)] = dW
    changes["db" + str(current_layer_index)] = db

    for current_layer_index in reversed(range(current_layer_index)):
        if current_layer_index == 0: break

        current_layer_history = forward_history["layer" + str(current_layer_index)]
        current_activation_function_deriv = network_architecture[current_layer_index]['deriv_activation']

        dW, db, curr_dA = single_layer_backprop(curr_dA, current_layer_history, current_activation_function_deriv)

        if not regul_labmda == 0:
            dW += regulziation_function(current_layer_history["weights"], regul_labmda)

        changes["dW" + str(current_layer_index)] = dW
        changes["db" + str(current_layer_index)] = db

    return changes



# recives the computed derivative of the current layer, the layer's forward history, and the relevant derivative function
# returns an array for this layer changes in weights, an array with this layer chanes in biases, and an array with the computed derivatives
# for the previous layer (previous in the graph, but the next one to be computed in the algorithm)
def single_layer_backprop(curr_dA, layer_history, der_activation):
    current_linear_output = layer_history['linear_output']
    outer_derivative = der_activation(current_linear_output)

    dZ = curr_dA * outer_derivative

    dW = np.dot(np.matrix(dZ).T, np.matrix(layer_history['input']))
    db = np.sum(dZ, keepdims=True)

    current_weights = layer_history['weights'].T
    prev_dA = current_weights.dot(dZ.T)

    return dW, db, prev_dA

# recives a dictionary containing the weights and biases for each layer, a dictionary containing the changes to be added
# to the weights and biases, and the learning rate(etta)
#
# returns the updated weights and biases according to the learning rate and the deltas
def update_weights_biases(weights_biases, weights_biases_deltas, learning_rate):
    num_layers = int(len(weights_biases) / 2)

    for layer_index in range(1,num_layers):
        weights_biases["W" + str(layer_index)] -= learning_rate * weights_biases_deltas["dW" + str(layer_index)]
        weights_biases["b" + str(layer_index)] -= learning_rate * weights_biases_deltas["db" + str(layer_index)]

    return weights_biases

def update_momentum_deltas(current_weights_deltas, previous_weights_deltas, momentum_alpha):
    num_layers = int(len(current_weights_deltas) / 2)

    for layer_index in range(1, num_layers):
        momentum_weights_delta = momentum_alpha * previous_weights_deltas["dW" + str(layer_index)]
        momentum_bias_delta = momentum_alpha * previous_weights_deltas["db" + str(layer_index)]
        current_weights_deltas["dW" + str(layer_index)] += momentum_weights_delta
        current_weights_deltas["db" + str(layer_index)] += momentum_bias_delta

    return current_weights_deltas


