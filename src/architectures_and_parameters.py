from helper_functions import *

# contains different options for the network's architecutre and hyper parameters



params_options = {
    "option1":  # avg accuracy 0.9745
    {
    'regul_lambda' : 0,
    'regul_function' : ridge_regression,
    'learning_rate' : 0.04,
    'momentum_rate' : 0.3,  # https://towardsdatascience.com/stochastic-gradient-descent-with-momentum-a84097641a5d
    'error_function' : cross_entropy,
    'der_error_function' : der_cross_entropy,
    'init_function' : Xavier_init,
    'num_iterations' : 250,
    'network_architecture' : (
        {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
        {'input_dim': 12, 'activation': tanh, 'deriv_activation': der_tanh},
        {'input_dim': 12, 'activation': tanh,'deriv_activation': der_tanh},
        {'input_dim': 12, 'activation': sigmoid,'deriv_activation': der_sigmoid},
        {'input_dim': 1},
        ),
     },
    # "option2":  # avg accuracy 0.958
    # {
    # 'regul_lambda' : 0.001,
    # 'regul_function' : ridge_regression,
    # 'learning_rate' : 0.04,
    # 'momentum_rate' : 0,  # https://towardsdatascience.com/stochastic-gradient-descent-with-momentum-a84097641a5d
    # 'error_function' : cross_entropy,
    # 'der_error_function' : der_cross_entropy,
    # 'init_function' : Xavier_init,
    # 'num_iterations' : 200,
    # 'network_architecture' : (
    #     {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': tanh, 'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': tanh,'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': sigmoid,'deriv_activation': der_sigmoid},
    #     {'input_dim': 1},
    #     ),
    #  },
    # "option3":  # avg accuracy 0.5495
    # {
    # 'regul_lambda' : 0.005,
    # 'regul_function' : LASSO,
    # 'learning_rate' : 0.04,
    # 'momentum_rate' : 0.4,  # https://towardsdatascience.com/stochastic-gradient-descent-with-momentum-a84097641a5d
    # 'error_function' : cross_entropy,
    # 'der_error_function' : der_cross_entropy,
    # 'init_function' : Xavier_init,
    # 'num_iterations' : 200,
    # 'network_architecture' : (
    #     {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': tanh, 'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': tanh,'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': sigmoid,'deriv_activation': der_sigmoid},
    #     {'input_dim': 1},
    #     ),
    #  },
    # "option4":  # avg accuracy 0.494
    # {
    # 'regul_lambda' : 0.01,
    # 'regul_function' : LASSO,
    # 'learning_rate' : 0.04,
    # 'momentum_rate' : 0,  # https://towardsdatascience.com/stochastic-gradient-descent-with-momentum-a84097641a5d
    # 'error_function' : cross_entropy,
    # 'der_error_function' : der_cross_entropy,
    # 'init_function' : Xavier_init,
    # 'num_iterations' : 200,
    # 'network_architecture' : (
    #     {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': tanh, 'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': tanh,'deriv_activation': der_tanh},
    #     {'input_dim': 12, 'activation': sigmoid,'deriv_activation': der_sigmoid},
    #     {'input_dim': 1},
    #     ),
    #  },
    # "option5":  # avg accuracy 0.59
    # {
    #     'regul_lambda': 0.05,
    #     'regul_function': ridge_regression,
    #     'learning_rate': 0.09,
    #     'momentum_rate': 0.4,
    #     'error_function': cross_entropy,
    #     'der_error_function': der_cross_entropy,
    #     'init_function': Lecun_init,
    #     'num_iterations': 150,
    #     'network_architecture': (
    #         {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #         {'input_dim': 12, 'activation': tanh, 'deriv_activation': der_tanh},
    #         {'input_dim': 12, 'activation': sigmoid, 'deriv_activation': der_sigmoid},
    #         {'input_dim': 1},
    #     )
    # },
    # "option6":  # avg accuracy 0.6164
    # {
    #     'regul_lambda': 0,
    #     'regul_function': ridge_regression,
    #     'learning_rate': 0.4,
    #     'momentum_rate': 0.6,
    #     'error_function': squared_error,
    #     'der_error_function': der_squared_error,
    #     'init_function': Xavier_init,
    #     'num_iterations': 150,
    #     'network_architecture': (
    #         {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #         {'input_dim': 12, 'activation': ReLU, 'deriv_activation': der_ReLU},
    #         {'input_dim': 12, 'activation': sigmoid, 'deriv_activation': der_sigmoid},
    #         {'input_dim': 1},
    #     )
    # },
    # "option7":  # avg accuracy 0.496
    # {
    #     'regul_lambda': 0.7,
    #     'regul_function': ridge_regression,
    #     'learning_rate': 0.35,
    #     'momentum_rate': 0.9,
    #     'error_function': cross_entropy,
    #     'der_error_function': der_cross_entropy,
    #     'init_function': Lecun_init,
    #     'num_iterations': 100,
    #     'network_architecture': (
    #         {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #         {'input_dim': 20, 'activation': leaky_ReLU, 'deriv_activation': der_leaky_ReLU},
    #         {'input_dim': 5, 'activation': sigmoid, 'deriv_activation': der_sigmoid},
    #         {'input_dim': 1},
    #     )
    # },
    # "option8":  # avg accuracy 0.498
    # {
    #     'regul_lambda': 0.9,
    #     'regul_function': LASSO,
    #     'learning_rate': 0.09,
    #     'momentum_rate': 0.03,
    #     'error_function': cross_entropy,
    #     'der_error_function': der_cross_entropy,
    #     'init_function': random_init,
    #     'num_iterations': 150,
    #     'network_architecture': (
    #     {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #     {'input_dim': 5, 'activation': tanh, 'deriv_activation': der_tanh},
    #     {'input_dim': 8, 'activation': tanh,'deriv_activation': der_tanh},
    #     {'input_dim': 5, 'activation': sigmoid,'deriv_activation': der_sigmoid},
    #     {'input_dim': 1},
    #     )
    # },
    # "option9":  # avg accuracy 0.64
    # {
    #     'regul_lambda': 0,
    #     'regul_function': ridge_regression,
    #     'learning_rate': 0.8,
    #     'momentum_rate': 0,
    #     'error_function': squared_error,
    #     'der_error_function': der_squared_error,
    #     'init_function': Lecun_init,
    #     'num_iterations': 100,
    #     'network_architecture': (
    #         {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #         {'input_dim': 12, 'activation': sigmoid, 'deriv_activation': der_sigmoid},
    #         {'input_dim': 1},
    #     )
    # },
    # "option10":  # avg accuracy:0.507
    # {
    #     'regul_lambda': 0.02,
    #     'regul_function': ridge_regression,
    #     'learning_rate': 0.3,
    #     'momentum_rate': 0.6,
    #     'error_function': cross_entropy,
    #     'der_error_function': der_cross_entropy,
    #     'init_function': Xavier_init,
    #     'num_iterations': 150,
    #     'network_architecture': (
    #         {'input_dim': 2, 'activation': tanh, 'deriv_activation': der_tanh},
    #         {'input_dim': 10, 'activation': leaky_ReLU, 'deriv_activation': der_leaky_ReLU},
    #         {'input_dim': 12, 'activation': tanh, 'deriv_activation': der_tanh},
    #         {'input_dim': 10, 'activation': sigmoid, 'deriv_activation': der_sigmoid},
    #         {'input_dim': 1},
    #     )
    # },
}






# # returns a dictionary of the network architecture, and a dictionary with the network hyper params
# def get_architecture_and_params(option):
#     hyper_parameters = {
#         "use_norm": use_normalization,
#         "norm_lambda": normalization_lambda,
#         # "normalization_function": normalization_function,
#         "learning_rate": learning_rate,
#         "momentum_rate": momentum_rate,
#         "error_function": error_function,
#         "der_error_function": der_error_function,
#         "init_function": init_function,
#         "num_iterations": num_iterations
#     }
#
#     return network_architecture, hyper_parameters
