import numpy as np
from collections.abc import Iterable



# ****** error functions:
def squared_error(sigma, sigma0):
    return 1/2 * ((np.subtract(sigma, sigma0)) ** 2)

def der_squared_error(sigma, sigma0):
    return np.subtract(sigma,sigma0)

def cross_entropy(sigma, sigma0):
    return -1/2 * (np.sum(sigma0 * np.log(sigma) + ((1 - sigma0) * np.log(1 - sigma))))

def der_cross_entropy(sigma, sigma0):
    return np.divide(sigma - sigma0, sigma * (1 - sigma))

# ******* activation functions(and derivatives):
def ReLU(x):
    if not isinstance(x, Iterable): #single value
        if x >= 0:
            return x
        return 0

    #array of values
    for i in range(len(x)):
        if x[i] < 0:
            x[i] = 0
    return x

def der_ReLU(x):
    if not isinstance(x, Iterable): #single value
        if x > 0:
            return 1
        return 0

    # array of values
    for i in range(len(x)):
        if x[i] > 0:
            x[i] = 1
        else:
            x[i] = 0
    return x

def leaky_ReLU(x):
    if not isinstance(x, Iterable): # single value
        if x >= 0:
            return x
        return x * 0.01

    # array of values
    for i in range(len(x)):
        if x[i] < 0:
            x[i] = x[i] * 0.01
    return x

def der_leaky_ReLU(x):
    if not isinstance(x, Iterable): # single value
        if x > 0:
            return 1
        return 0.01

    # array of values
    for i in range(len(x)):
        if x[i] > 0:
            x[i] = 1
        else:
            x[i] = 0.01
    return x

def tanh(x):
    return np.tanh(x)

def der_tanh(x):
    # if not isinstance(x, Iterable): #single value
    #     return 1 - (tanh(x) ** 2)
    #
    # # array of values
    # for i in range(len(x)):
    #     x[i] = 1 - (tanh(x[i]) ** 2)
    # return x
    return 1 - (np.power(tanh(x), 2))

def sigmoid(x):
    # if not isinstance(x, Iterable): # single value
    #     return 1/2 * (1 + tanh(x))
    #
    # # array of values
    # for i in range(len(x)):
    #     x[i] = 1/2 * (1 + tanh(x[i]))
    # return x

    return 1/2 * (1 + tanh(x))

def der_sigmoid(x):
    # if not isinstance(x, Iterable): # single value
    #     return sigmoid(x) * (1 - sigmoid(x))
    #
    # # array of values
    # for i in range(len(x)):
    #     x[i] = sigmoid(x[i]) * (1 - sigmoid(x[i]))
    # return x

    return sigmoid(x) * (1 - sigmoid(x))


# ****** regularization types:
def LASSO(weights ,normalization_lambda): #L1
    ans = normalization_lambda * np.sign((weights))
    return 1 * ans

def ridge_regression(weights, normalization_lambda): #L2

    return normalization_lambda * weights


# ****** init functions:
def Lecun_init(network_architecture):
    num_of_layers = len(network_architecture) - 1

    weights_biases = {}

    for layer_idx in range(num_of_layers):
        num_output = network_architecture[layer_idx+1]['input_dim']
        num_input = network_architecture[layer_idx]['input_dim']

        layer_weights = np.random.normal(loc=0, scale=np.sqrt(1/num_input), size=(num_output, num_input))
        layer_biases = np.zeros((num_output, 1))

        weights_biases["W" + str(layer_idx + 1)] = layer_weights
        weights_biases["b" + str(layer_idx + 1)] = layer_biases

    return weights_biases


def Xavier_init(network_architecture):
    num_of_layers = len(network_architecture) - 1

    weights_biases = {}

    for layer_idx in range(num_of_layers):
        num_output = network_architecture[layer_idx+1]['input_dim']
        num_input = network_architecture[layer_idx]['input_dim']

        layer_weights = np.random.normal(loc=0, scale=np.sqrt(2/(num_input + num_output)), size=(num_output, num_input))
        layer_biases = np.zeros((num_output, 1))

        weights_biases["W" + str(layer_idx + 1)] = layer_weights
        weights_biases["b" + str(layer_idx + 1)] = layer_biases

    return weights_biases


def random_init(network_architecture):
    num_of_layers = len(network_architecture) - 1

    weights_biases = {}

    for layer_idx in range(num_of_layers):
        num_output = network_architecture[layer_idx+1]['input_dim']
        num_input = network_architecture[layer_idx]['input_dim']

        layer_weights = np.random.randn(num_output, num_input)
        layer_biases = np.zeros((num_output, 1))

        weights_biases["W" + str(layer_idx + 1)] = layer_weights
        weights_biases["b" + str(layer_idx + 1)] = layer_biases

    return weights_biases
